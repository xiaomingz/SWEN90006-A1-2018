package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{

	
  private List<String> readInstructions(String file)
  {
	Charset charset = Charset.forName("UTF-8");
	List<String> lines = null;
	try {
	  lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
	}
	catch (Exception e){
	  System.err.println("Invalid input file! (stacktrace follows)");
	  e.printStackTrace(System.err);
	  System.exit(1);
	}
	return lines;
  }
  @Test
  public void boundaryTest1() throws Exception {
	  final List<String> lines = readInstructions("examples/BV1.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), -65535);
  }
  @Test
  public void boundaryTest2() throws Exception {
	  final List<String> lines = readInstructions("examples/BV2.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 65535);
  }
  @Test()
  public void boundaryTest3() throws Exception {
	  final List<String> lines = readInstructions("examples/BV3.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }
  }
  @Test
  public void boundaryTest4() throws Exception {
	  final List<String> lines = readInstructions("examples/BV4.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }
  }
  @Test
  public void boundaryTest5() throws Exception {
	  final List<String> lines = readInstructions("examples/BV5.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2);
  }
  @Test
  public void boundaryTest6() throws Exception {
	  final List<String> lines = readInstructions("examples/BV6.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2);
  }
  @Test
  public void boundaryTest7() throws Exception {
	  final List<String> lines = readInstructions("examples/BV7.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }
  }
  @Test
  public void boundaryTest8() throws Exception {
	  final List<String> lines = readInstructions("examples/BV8.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }
  }
  @Test
  public void boundaryTest9() throws Exception {
	  final List<String> lines = readInstructions("examples/BV9.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), -2147483648);
  }
  @Test
  public void boundaryTest10() throws Exception {
	  final List<String> lines = new ArrayList<String>();
	  lines.add("MOV R0 65535");
	  lines.add("MOV R1 32768");
	  lines.add("MOV R2 32767");
	  lines.add("MUL R0 R0 R1");
	  lines.add("ADD R0 R0 R2");
	  lines.add("RET R0");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2147483647);
  }
  @Test
  public void boundaryTest11() throws Exception {
	  final List<String> lines = readInstructions("examples/BV11.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2147483647);
  }
  @Test
  public void boundaryTest12() throws Exception {
	  final List<String> lines = readInstructions("examples/BV12.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), -2147483648);
  }
  @Test
  public void boundaryTest13() throws Exception {
	  final List<String> lines = readInstructions("examples/BV13.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 0);
  }
  @Test
  public void boundaryTest14() throws Exception {
	  final List<String> lines = readInstructions("examples/BV14.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 0);
  }
  @Test
  public void boundaryTest15() throws Exception {
	  final List<String> lines = readInstructions("examples/BV15.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }
  }
  @Test
  public void boundaryTest16() throws Exception {
	  final List<String> lines = readInstructions("examples/BV16.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 5);

  }
  @Test
  public void boundaryTest17() throws Exception {
	  final List<String> lines = readInstructions("examples/BV17.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2);
  }
  @Test
  public void boundaryTest18() throws Exception {
	  final List<String> lines = readInstructions("examples/BV18.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2);
  }
  @Test
  public void boundaryTest19() throws Exception {
	  final List<String> lines = readInstructions("examples/BV19.s");
	    Machine m = new Machine();
	   assertEquals(m.execute(lines), -2);
  }
  @Test
  public void boundaryTest20() throws Exception {
	  final List<String> lines = readInstructions("examples/BV20.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 0);
  }
  @Test
  public void boundaryTest21() throws Exception {
	  final List<String> lines = readInstructions("examples/BV21.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 1);
  }
  @Test
  public void boundaryTest22() throws Exception {
	  final List<String> lines = readInstructions("examples/BV22.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof NoReturnValueException);  
	    }
  }
  @Test
  public void boundaryTest23() throws Exception {
	  final List<String> lines = readInstructions("examples/BV23.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof NoReturnValueException);  
	    }
  }
}
