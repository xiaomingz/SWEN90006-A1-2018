package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  private List<String> readInstructions(String file){
   Charset charset = Charset.forName("UTF-8");
   List<String> lines = null;
	try {
		 lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
	}
	catch (Exception e){
		 System.err.println("Invalid input file! (stacktrace follows)");
		  e.printStackTrace(System.err);
		  System.exit(1);
		}
		return lines;
	 }
  @Test
  public void partitionTest1() throws Exception{
	  final List<String> lines = readInstructions("examples/EC1.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 10);
    
  }
  @Test
  public void partitionTest2() throws Exception{
	  final List<String> lines = readInstructions("examples/EC2.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof NoReturnValueException);  
	    }
  }
  @Test
  public void partitionTest3() throws Exception{
	  final List<String> lines = readInstructions("examples/EC3.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof NoReturnValueException);  
	    }
  }
  @Test
  public void partitionTest4() throws Exception{
	  final List<String> lines = readInstructions("examples/EC4.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof NoReturnValueException);  
	    }
  }
  @Test
  public void partitionTest5() throws Exception{
	  final List<String> lines = readInstructions("examples/EC5.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }

  }
  @Test
  public void partitionTest6() throws Exception{
	  final List<String> lines = readInstructions("examples/EC6.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }

  }
  @Test
  public void partitionTest7() throws Exception{
	  final List<String> lines = readInstructions("examples/EC7.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 2147483647);

  }
  @Test
  public void partitionTest8() throws Exception{
	  final List<String> lines = readInstructions("examples/EC8.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), -2147483648);

  }
  @Test
  public void partitionTest9() throws Exception{
	  final List<String> lines = readInstructions("examples/EC9.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }


  }
  @Test
  public void partitionTest10() throws Exception{
	  final List<String> lines = readInstructions("examples/EC10.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }


  }
  @Test
  public void partitionTest11() throws Exception{
	  final List<String> lines = readInstructions("examples/EC11.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }

  }
  @Test
  public void partitionTest12() throws Exception{
	  final List<String> lines = readInstructions("examples/EC12.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }


  }
  @Test
  public void partitionTest131() throws Exception{
	  final List<String> lines = readInstructions("examples/EC13-1.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }


  }
  @Test
  public void partitionTest132() throws Exception{
	  final List<String> lines = readInstructions("examples/EC13-2.s");
	    Machine m = new Machine();
	    try {
	    	m.execute(lines);
	    }catch(Exception e) {
	    	 assertTrue(e instanceof InvalidInstructionException);  
	    }


  }
  @Test
  public void partitionTest14() throws Exception{
	  final List<String> lines = readInstructions("examples/EC14.s");
	    Machine m = new Machine();
	    assertEquals(m.execute(lines), 1);

  }
}
